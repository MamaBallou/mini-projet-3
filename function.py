import sqlite3
import csv
import os

def upsert(bdd, fich_csv):
    list_ajout = [],[]
    list_ajout = decoupage(fich_csv)
    connection = sqlite3.connect(bdd)
    cursor = connection.cursor()
    cursor.execute("""
    create table if not exists Voitures
        (adresse_titulaire text, 
        nom text, 
        prenom text,
        immatriculation text,
        date_immatriculation text, 
        vin text, 
        marque text,
        denomination_commerciale text,
        couleur text, 
        carrosserie text, 
        categorie text, 
        cylindree text, 
        energie text, 
        places text, 
        poids text,
        puissance text,
        type text,
        variante text, 
        version text );
    """)
    resultat = ["adresse_titulaire","nom","prenom","immatriculation","date_immatriculation","vin","marque","denomination_commerciale","couleur","carrosserie","categorie","cylindree","energie","places","poids","puissance","type","variante","version"]
    for compteur in range(len(list_ajout)) :
        cursor.execute("select count(*) from Voitures where immatriculation = ?", (list_ajout[compteur][3], ))
        value=cursor.fetchall()
        if(value[0][0]==0):

            a_execute = insert(list_ajout, compteur,bdd)
            cursor.execute(a_execute)
            connection.commit()

        else : 
            entree = "update Voitures set "
            entree = entree + resultat[0] + '=' + "'" + list_ajout[compteur][0] + "'"
            for val in range(18):
                entree+=','
                entree+=resultat[val+1] + "=" + "'" + list_ajout[compteur][val+1] + "'"
            entree+="where immatriculation = '" + list_ajout[compteur][3] + "';"
            cursor.execute(entree)
            connection.commit()


        


def insert(liste_ajout, numero_ligne, bdd) :
    connection = sqlite3.connect(bdd, isolation_level=None)
    cursor = connection.cursor()
    taille = len(liste_ajout)
    entree = "insert into Voitures values ('"
    entree += liste_ajout[numero_ligne][0]
    entree+="'"
    for val2 in range(18) :
        entree+=','
        entree+="'"
        entree+=liste_ajout[numero_ligne][val2+1]
        entree+="'"
    entree+=');'
    return entree


def decoupage(fic) :
    #Initialise le type de la variable
    tabFinal = []
    #Ouvre le fichier
    with open(fic, newline='') as csvFile :
        reader = csv.reader(csvFile, delimiter=';')
        for row in reader :
            tabFinal.append(row)
        #On retire l'en-tête
        if tabFinal :
            tabFinal.pop(0)
    #On ferme le fichier
    csvFile.close()
    return tabFinal