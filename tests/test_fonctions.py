import unittest
import sqlite3
import os
import csv
from function import upsert
from verif import verifArg

class ImportTest(unittest.TestCase):
    def setUp(self):
        self.connection = sqlite3.connect('test.sqlite3')
        self.cursor = self.connection.cursor()
        self.cursor.execute('''
        create table Voitures
        (adresse_titulaire text, 
        nom text, 
        prenom text,
        immatriculation text);
        ''')
        self.connection.commit()

    def tearDown(self):
        self.cursor.execute('''drop table Voitures''')
        self.connection.commit()
        self.connection.close()
        os.remove('test.sqlite3')

    def test_verifArg(self):
        self.assertFalse(verifArg(2, ['prog.py', 'test.csv']))
        self.assertFalse(verifArg(3, ['prog.py', 'test.txt', 'monfichier']))
        self.assertFalse(verifArg(3, ['prog.py', 'test.csv', 'monfichier']))
        ## Création test.csv
        file = open('test.csv', 'wb')
        file.close()
        self.assertTrue(verifArg(3, ['prog.py', 'test.csv', 'monfichier']))
        os.remove('test.csv')

    def test_upsert(self):
        value = 0
        self.cursor.execute("select count(*) from Voitures")
        resul = self.cursor.fetchall()
        self.assertEqual(resul[0][0], 0)
        liste =['26 rue du labrador','Clérentin','Arnaud', 'A7E']
        upsert('test.sqlite3', liste)
        self.cursor.execute("select * from Voitures")
        resultat = self.cursor.fetchall()
        self.assertEqual(resultat,'26 rue du labrador Clérentin Arnaud A7E')

        