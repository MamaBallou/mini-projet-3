import sys
from verif import verifArg
from function import *

verification = verifArg(len(sys.argv),sys.argv)

if verification == False :
    print("Erreur : Arguments invalide (il faut : la commande - le fichier csv -)")
    exit (-1)

upsert('Voitures.sqlite3', sys.argv[1])

exit(0)